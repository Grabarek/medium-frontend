import types from './types';

const INITIAL_STATE = {
    appName: 'Medium',
    appLoaded: false,
    token: null,
    viewChangeCounter: 0
};

const commons_reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.APP_LOAD:
            return {
                ...state,
                token: action.token || null,
                appLoaded: true,
                currentUser: action.payload ? action.payload.user : null
            };
        case types.REDIRECT:
            return {
                ...state,
                redirectTo: null
            };
        case types.SETTINGS_SAVED:
            return {
                ...state,
                redirectTo: action.error ? null : '/',
                currentUser: action.error ? null : action.payload.user
            };
        case types.LOGIN:
        case types.REGISTER:
            return {
                ...state,
                redirectTo: action.error ? null : '/',
                token: action.error ? null : action.payload.user.token,
                currentUser: action.error ? null : action.payload.user
            };
        case types.LOGOUT:
            return {
                ...state,
                redirectTo: '/',
                token: null,
                currentUser: null
            };
        default:
            return state;
    }
};

export default commons_reducer;
