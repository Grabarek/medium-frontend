import * as Yup from 'yup';

const SettingsValidationSchema = Yup.object().shape({
    image: Yup.string()
        .min(3, 'Url is too short')
        .max(150, 'Url is too long!')
        .matches(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/, 'Invalid url address')
    ,
    username: Yup.string()
        .required('Username is required')
        .min(3, 'Username too short')
        .max(50, 'Username too long!')
    ,
    email: Yup.string()
        .required('E-mail address is required')
        .email('Invalid email address')
    ,
    bio: Yup.string()
        .max(100, 'Bio is too long!')
    ,
    password: Yup.string()
        .min(8, 'Password has to be at least 8 characters long')
        .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/, 'Password has to contain at least one letter and one number')
});

export default SettingsValidationSchema
