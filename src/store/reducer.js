import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'

import commons_reducer from './commons/reducers';
import auth_reducer from './auth/reducers';
import settings_reducer from './settings/reducers';

const createRootReducer = (history) => combineReducers({
    router: connectRouter(history),
    commons: commons_reducer,
    auth: auth_reducer,
    settings: settings_reducer
});

export default createRootReducer
