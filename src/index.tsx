import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router'
import configureStore, {history} from './store/store'

import './normalize.scss';
import './globals.scss';
import App from './components/App/App';

const store = configureStore();


ReactDOM.render((
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>

), document.getElementById('root'));
