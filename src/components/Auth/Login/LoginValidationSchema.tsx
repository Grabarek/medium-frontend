import * as Yup from 'yup';

const LoginValidationSchema = Yup.object().shape({
    email: Yup.string()
        .required('E-mail address is required'),
    password: Yup.string()
        .required('Password is required')
});

export default LoginValidationSchema
