import React, {useState, useEffect} from 'react';
import {Formik, Form, Field} from 'formik';
import {Link} from 'react-router-dom';
import {connect, useDispatch} from 'react-redux';
import {onLogin, onLoginUnload} from '../../../store/auth/actions';

import ListErrors from "../ListErrors/ListErrors";
import LoginValidationSchema from "./LoginValidationSchema";
import passwordIcon from '../../../assets/icons/eye.svg';
import './Login.scss';

const mapStateToProps = state => ({
    ...state.auth
});

const mapDispatchToProps = () => ({
    onLogin,
    onLoginUnload
});

const Login = ({errors}) => {
    const dispatch = useDispatch();
    const [passwordVisibility, setPasswordVisibility] = useState(false);

    useEffect(() => () => dispatch(onLoginUnload()), [] );


    return (
        <div className="login">
            <div className="container">
                <div className="login__wrapper">
                    <h1 className="login__title">Sign in</h1>
                    <Link to="/register">
                        <p className="login__redirect">
                            Need an account?
                        </p>
                    </Link>

                    <ListErrors errors={errors} />

                    <Formik
                        initialValues={{
                            email: '',
                            password: ''
                        }}
                        validationSchema={LoginValidationSchema}
                        onSubmit={values => {
                            dispatch(onLogin(values))
                        }}
                    >
                        {({errors, touched}) => (
                            <Form className="login__form">
                                <div className="login__form-row">
                                    <Field
                                        name="email"
                                        placeholder="E-mail address"
                                    />
                                    {errors.email && touched.email &&
                                    <div className="login__form-error">{errors.email}</div>}
                                </div>

                                <div className="login__form-row">
                                    <img
                                        src={passwordIcon}
                                        alt="reveal password"
                                        className={`password__reveal ${passwordVisibility && 'active'}`}
                                        onClick={() => setPasswordVisibility(!passwordVisibility)}
                                    />
                                    <Field
                                        name="password"
                                        placeholder="Password"
                                        type={passwordVisibility ? 'text' : 'password'}
                                    />
                                    {errors.password && touched.password &&
                                    <div className="login__form-error">{errors.password}</div>}
                                </div>

                                <button type="submit" className="login__form-submit">Submit</button>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    )

};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
