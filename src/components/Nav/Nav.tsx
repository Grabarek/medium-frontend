import React from 'react';
import { Link } from 'react-router-dom';

import LoggedInView from './LoggedInView/LoggedInView';
import LoggedOutView from './LoggedOutView/LoggedOutView';

import logo from '../../assets/images/logo.svg';
import './Nav.scss';


const Nav = ({currentUser}) => (

    <nav className="nav">
        <div className="container">
            <div className="nav__wrapper">
                <div className="nav__left">
                    <Link to="/" className="nav__link">
                        <img src={logo} alt="logo" className="nav__logo" />
                    </Link>
                </div>
                {currentUser ?
                    <LoggedInView currentUser={currentUser} />
                    :
                    <LoggedOutView />
                }
            </div>
        </div>
    </nav>
);

export default Nav;
