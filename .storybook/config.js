// add global styles
import '!style-loader!css-loader!sass-loader!../src/normalize.scss';
import '!style-loader!css-loader!sass-loader!../src/globals.scss';

import React from 'react';
import {addDecorator} from '@storybook/react';
import {Provider as StoreProvider} from 'react-redux';

import configureStore, {history} from '../src/store/store'

const store = configureStore();

// add global styles
addDecorator(story =>
    <>
        <StoreProvider store={store}>
                {story()}
        </StoreProvider>
    </>
);
