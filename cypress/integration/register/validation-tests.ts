describe('Register component validation tests', function () {

    beforeEach(() => {
        cy
            .visit('http://localhost:3001/register')
            .get('input').clear()

    });

    it('Renders register component', function () {
        cy
            .visit('http://localhost:3001/register')
            .get('.register');
    });

    //username input
    it('Renders username input field', () => {
        cy.get('input[name="username"]')
    });

    it('Displays error message if username input is empty', () => {
        cy
            .get('input[name="username"]')
            .should('be.empty')
            .get('button[type="submit"]')
            .click()
            .get('input[name="username"]')
            .siblings('.register__form-error')
            .should('have.text', 'Username is required');
    });

    it('Displays error message if username value has less than 3 characters', () => {
        cy
            .get('input[name="username"]')
            .type('ab')
            .get('button[type="submit"]')
            .click()
            .get('input[name="username"]')
            .siblings('.register__form-error')
            .should('have.text', 'Username too short');
    });

    it('Does not display error message if username value has at least 3 characters', () => {
        cy
            .get('input[name="username"]')
            .type('abc')
            .get('button[type="submit"]')
            .click()
            .get('input[name="username"]')
            .siblings('.register__form-error')
            .should('not.exist');
    });

    it('Displays error message if username value has more than 15 characters', () => {
        cy
            .get('input[name="username"]')
            .type('abcdefghijklmnop')
            .get('button[type="submit"]')
            .click()
            .get('input[name="username"]')
            .siblings('.register__form-error')
            .should('have.text', 'Username too long');
    });

    it('Does not display error message if username value has at most 15 characters', () => {
        cy
            .get('input[name="username"]')
            .type('123123123123123')
            .get('button[type="submit"]')
            .click()
            .get('input[name="username"]')
            .siblings('.register__form-error')
            .should('not.exist');
    });

    it('Displays error message if username contains special characters', () => {
        cy
            .get('input[name="username"]')
            .type('Username$')
            .get('button[type="submit"]')
            .click()
            .get('input[name="username"]')
            .siblings('.register__form-error')
            .should('have.text', 'Username can contain only letters and numbers');
    });


    //email input
    it('Renders email input field', () => {
        cy.get('input[name="email"]')
    });

    it('Displays error message if email input is empty', () => {
        cy
            .get('input[name="email"]')
            .should('be.empty')
            .get('button[type="submit"]')
            .click()
            .get('input[name="email"]')
            .siblings('.register__form-error')
            .should('have.text', 'E-mail address is required');
    });


    it('Displays error message if email scheme is incorrect', () => {
        cy
            .get('input[name="email"]')
            .type('testgmail.com')
            .get('button[type="submit"]')
            .click()
            .get('input[name="email"]')
            .siblings('.register__form-error')
            .should('have.text', 'Invalid email address');
    });


    it('Does not display error message if email scheme is correct', () => {
        cy
            .get('input[name="email"]')
            .type('test@gmail.com')
            .get('button[type="submit"]')
            .click()
            .get('input[name="email"]')
            .siblings('.register__form-error')
            .should('not.exist');
    });


    //password input
    it('Renders password input field', () => {
        cy.get('input[name="password"]')
    });

    it('Displays error message if password input is empty', () => {
        cy
            .get('input[name="password"]')
            .should('be.empty')
            .get('button[type="submit"]')
            .click()
            .get('input[name="password"]')
            .siblings('.register__form-error')
            .should('have.text', 'Password is required');
    });

    it('Displays error message if password value has less than 8 characters', () => {
        cy
            .get('input[name="password"]')
            .type('12')
            .get('button[type="submit"]')
            .click()
            .get('input[name="password"]')
            .siblings('.register__form-error')
            .should('have.text', 'Password has to be at least 8 characters long');
    });

    it('Displays error message if password value contains only numbers', () => {
        cy
            .get('input[name="password"]')
            .type('1234343424')
            .get('button[type="submit"]')
            .click()
            .get('input[name="password"]')
            .siblings('.register__form-error')
            .should('have.text', 'Password has to contain at least one letter and one number');
    });

    it('Displays error message if password value contains only letters', () => {
        cy
            .get('input[name="password"]')
            .type('password')
            .get('button[type="submit"]')
            .click()
            .get('input[name="password"]')
            .siblings('.register__form-error')
            .should('have.text', 'Password has to contain at least one letter and one number');
    });

    it('Displays error message if password value contains only special characters', () => {
        cy
            .get('input[name="password"]')
            .type('!@#$%^&*')
            .get('button[type="submit"]')
            .click()
            .get('input[name="password"]')
            .siblings('.register__form-error')
            .should('have.text', 'Password has to contain at least one letter and one number');
    });

    it('Does not display error message if password value has at least 8 characters and contains both letters and numbers', () => {
        cy
            .get('input[name="password"]')
            .type('password123')
            .get('button[type="submit"]')
            .click()
            .get('input[name="password"]')
            .siblings('.register__form-error')
            .should('not.exist');
    });


});
