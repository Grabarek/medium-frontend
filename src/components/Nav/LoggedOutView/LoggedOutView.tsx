import React from 'react';
import { Link } from 'react-router-dom';

const LoggedOutView = () => (
    <div className="nav__right">
        <ul className="nav__items">
            <li className="nav__item">
                <Link to="/login" className="nav__link">
                    Sign in
                </Link>
            </li>
            <li className="nav__item">
                <Link to="/register" className="nav__link">
                    Sign up
                </Link>
            </li>
        </ul>
    </div>
);

export default LoggedOutView;
