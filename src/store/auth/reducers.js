import types from './types';

const INITIAL_STATE = {};

const auth_reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.LOGIN:
        case types.REGISTER:
            return {
                ...state,
                inProgress: false,
                errors: action.error ? action.payload.errors : null
            };
        case types.LOGIN_PAGE_UNLOADED:
        case types.REGISTER_PAGE_UNLOADED:
            return {};
        case types.ASYNC_START:
            if (action.subtype === types.LOGIN || action.subtype === types.REGISTER) {
                return {
                    ...state,
                    inProgress: true
                };
            }
            else {
                return null
            }
            break;
        default:
            return state;
    }
};

export default auth_reducer;
