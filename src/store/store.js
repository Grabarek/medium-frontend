import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { promiseMiddleware, localStorageMiddleware } from './middleware';
import createRootReducer from './reducer'
import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from "history";
export const history = createBrowserHistory();


export default function configureStore(preloadedState) {
    const store = createStore(
        createRootReducer(history),
        preloadedState,
        composeWithDevTools(
            applyMiddleware(
                routerMiddleware(history),
                promiseMiddleware,
                localStorageMiddleware,
                createLogger()
            ),
        ),
    );

    //allow Cypress to access store values
    if (window.Cypress) {
        window.store = store;
    }

    return store;
}
