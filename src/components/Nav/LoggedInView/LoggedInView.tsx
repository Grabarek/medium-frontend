import React from 'react';
import { Link } from 'react-router-dom';

import addPostIcon from '../../../assets/icons/add-post.svg';
import settingsIcon from '../../../assets/icons/settings.svg';
import profileIcon from '../../../assets/icons/profile.svg';

interface NavProps {
    currentUser: {
        username: string,
        image: string
    }
}

const addDefaultSrc = (ev) => {
    ev.target.src = profileIcon;
    ev.target.onerror = null;
};

const LoggedInView: React.FC<NavProps> = ({currentUser}) => (
    <div className="nav__right">
        <ul className="nav__items">
            <li className="nav__item">
                <Link to="/editor" className="nav__link">
                    <img
                        src={addPostIcon}
                        alt="new post icon"
                        className="nav__icon"
                    />
                    <span>New post</span>
                </Link>
            </li>
            <li className="nav__item">
                <Link to="/settings" className="nav__link">
                    <img
                        src={settingsIcon}
                        alt="new post icon"
                        className="nav__icon"
                    />
                    <span>Settings</span>
                </Link>
            </li>
            <li className="nav__item">
                <Link to={`@${currentUser.username}`} className="nav__link">
                    <img
                        src={currentUser.image ? currentUser.image : profileIcon}
                        className="nav__icon"
                        onError={(e) => addDefaultSrc(e)}
                    />
                    <span>{currentUser.username}</span>
                </Link>
            </li>
        </ul>
    </div>
);

export default LoggedInView;
