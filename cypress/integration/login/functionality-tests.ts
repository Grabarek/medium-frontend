describe('Login component functionality tests', function () {

    it('Should display message if provided data is wrong', function () {
        cy
            .visit('http://localhost:3001/login')
            .get('.login')
            .get('input[name="email"]')
            .type('loremipsum@gmail.com')
            .should('have.value', 'loremipsum@gmail.com')
            .get('input[name="password"]')
            .type('PasswordText123')
            .should('have.value', 'PasswordText123')
            .get('button[type="submit"]')
            .click()
            .get('.error__messages')
            .contains('is invalid')
            .url().should('include', 'login')
    });

    it('Should keep inputs filled after receiving an error message', function () {
        cy
            .get('input[name="email"]')
            .invoke('val').should('not.be.empty')
            .get('input[name="password"]')
            .invoke('val').should('not.be.empty')
    });

    it('Should successfully login without errors', function () {
        cy
            .visit('http://localhost:3001/login')
            .get('.login')
            .get('input[name="email"]')
            .type('test@gmail.com')
            .should('have.value', 'test@gmail.com')
            .get('input[name="password"]')
            .type('PasswordText123')
            .should('have.value', 'PasswordText123')
            .get('button[type="submit"]')
            .click()
            .get('.login__form-error')
            .should('not.exist')
            .get('.error__messages')
            .should('not.exist')
    });

    it('Should automatically redirect to homepage on login', function () {
        cy
            .url().should('not.include', 'login')
            .get('.home')
    });

    it('Should keep user data in store', function () {
        cy
            .wait(1000)
            .window()
            .its('store')
            .invoke('getState')
            .its('commons.currentUser.username')
            .should('exist')
    });


});
