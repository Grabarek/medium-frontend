import React from 'react';
import { storiesOf } from '@storybook/react';
import {BrowserRouter} from 'react-router-dom';

import Nav from '../Nav/Nav';
import logo from '../../assets/images/logo.svg';
import profileIcon from '../../assets/icons/profile.svg';
import './Nav.scss';

const mockedUser = {
    username: 'Jan Kowalski',
    image: profileIcon
};

storiesOf('Nav', module)
    .addDecorator(story => (
        <BrowserRouter>
        {story()}
        </BrowserRouter>
    ))
    .add('Nav logged out', () => <Nav currentUser={null} />)
    .add('Nav logged in', () => <Nav currentUser={mockedUser} />)
;





