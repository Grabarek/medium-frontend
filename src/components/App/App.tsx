import React, {useEffect} from 'react';
import { Route, Switch, withRouter} from 'react-router-dom';
import {connect, useDispatch} from 'react-redux';
import { push } from 'react-router-redux';
import {onLoad, onRedirect} from '../../store/commons/actions';
import agent from '../../agent';

import Nav from '../Nav/Nav';
import Home from '../Home/Home';
import Login from '../Auth/Login/Login';
import Register from '../Auth/Register/Register';
import Settings from '../Settings/Settings';

import './App.scss';

import configureStore from '../../store/store'
const store = configureStore();

const mapStateToProps = state => {
    return {
        appLoaded: state.commons.appLoaded,
        currentUser: state.commons.currentUser,
        redirectTo: state.commons.redirectTo
    }
};

const mapDispatchToProps = () => ({
    onLoad,
    onRedirect
});

const App = ({appLoaded, currentUser, redirectTo}) => {
    const dispatch = useDispatch();

    //dispatch route redirect if present
    useEffect(() => {
        if(redirectTo) {
            store.dispatch(push(redirectTo));
        }
    },[redirectTo]);

    //set token and auth data
    useEffect(() => {
        const token = window.localStorage.getItem('jwt');
        if (token) {
            agent.setToken(token);
        }

        dispatch(onLoad(token ? agent.Auth.current() : null, token));
    }, []);

    return (
        appLoaded && (
            <>
                <Nav currentUser={currentUser}/>
                <div className="content">
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/register" component={Register}/>
                        <Route path="/settings" component={Settings}/>
                    </Switch>
                </div>

            </>
        )
    )
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
