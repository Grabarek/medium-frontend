describe('Login component validation tests', function () {

    beforeEach(() => {
        cy
            .visit('http://localhost:3001/login')
            .get('input').clear()

    });

    it('Renders login component', function () {
        cy
            .visit('http://localhost:3001/login')
            .get('.login');
    });


    //email input
    it('Renders email input field', () => {
        cy.get('input[name="email"]')
    });

    it('Displays error message if email input is empty', () => {
        cy
            .get('input[name="email"]')
            .should('be.empty')
            .get('button[type="submit"]')
            .click()
            .get('input[name="email"]')
            .siblings('.login__form-error')
            .should('have.text', 'E-mail address is required');
    });


    //password input
    it('Renders password input field', () => {
        cy.get('input[name="password"]')
    });

    it('Displays error message if password input is empty', () => {
        cy
            .get('input[name="password"]')
            .should('be.empty')
            .get('button[type="submit"]')
            .click()
            .get('input[name="password"]')
            .siblings('.login__form-error')
            .should('have.text', 'Password is required');
    });


});
