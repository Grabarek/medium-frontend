import React, {useState, useEffect} from 'react';
import {Formik, Form, Field} from 'formik';
import {Link} from 'react-router-dom';
import {connect, useDispatch} from 'react-redux';
import {onRegister, onRegisterUnload} from '../../../store/auth/actions';

import ListErrors from '../ListErrors/ListErrors';
import RegisterValidationSchema from "./RegisterValidationSchema";
import passwordIcon from '../../../assets/icons/eye.svg';
import './Register.scss';

const mapStateToProps = state => ({
    ...state.auth
});

const mapDispatchToProps = () => ({
    onRegister,
    onRegisterUnload
});

interface RegisterFormValues {
    username: string,
    email: string,
    password: string
}

const Register = ({errors}) => {
    const dispatch = useDispatch();
    const [passwordVisibility, setPasswordVisibility] = useState(false);

    const initialValues: RegisterFormValues = {
        username: '',
        email: '',
        password: ''
    };

    useEffect(() => () => dispatch(onRegisterUnload()), [] );

    return (
        <div className="register">
            <div className="container">
                <div className="register__wrapper">
                    <h1 className="register__title">Sign up</h1>
                    <Link to="/login">
                        <p className="register__redirect">
                            Already an user?
                        </p>
                    </Link>

                    <ListErrors errors={errors} />

                    <Formik
                        initialValues={initialValues}
                        validationSchema={RegisterValidationSchema}
                        onSubmit={values => {
                            dispatch(onRegister(values))
                        }}
                    >
                        {({errors, touched}) => (
                            <Form className="register__form">
                                <div className="register__form-row">
                                    <Field
                                        name="username"
                                        placeholder="Username"
                                    />
                                    {errors.username && touched.username &&
                                    <div className="register__form-error">{errors.username}</div>}
                                </div>

                                <div className="register__form-row">
                                    <Field
                                        name="email"
                                        placeholder="E-mail address"
                                    />
                                    {errors.email && touched.email &&
                                    <div className="register__form-error">{errors.email}</div>}
                                </div>

                                <div className="register__form-row">
                                    <img
                                        src={passwordIcon}
                                        alt="reveal password"
                                        className={`password__reveal ${passwordVisibility && 'active'}`}
                                        onClick={() => setPasswordVisibility(!passwordVisibility)}
                                    />
                                    <Field
                                        name="password"
                                        placeholder="Password"
                                        type={passwordVisibility ? 'text' : 'password'}
                                    />
                                    {errors.password && touched.password &&
                                    <div className="register__form-error">{errors.password}</div>}
                                </div>

                                <button type="submit" className="register__form-submit">Submit</button>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    )

};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
