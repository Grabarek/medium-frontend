import types from './types.js';
import agent from '../../agent';

const onRegister = ({username, email, password}) => {
    const payload = agent.Auth.register(username, email, password);

    return {
        type: types.REGISTER,
        payload
    }
};

const onRegisterUnload = () => {
  return {
      type: types.REGISTER_PAGE_UNLOADED
  }
};

const onLogin = ({email, password}) => {
    const payload = agent.Auth.login(email, password);

    return {
        type: types.LOGIN,
        payload
    }
};

const onLoginUnload = () => {
    return {
        type: types.LOGIN_PAGE_UNLOADED
    }
};

export {
    onRegister,
    onRegisterUnload,
    onLogin,
    onLoginUnload
}
