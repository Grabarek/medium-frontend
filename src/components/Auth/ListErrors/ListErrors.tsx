import React from 'react';
import './ListErrors.scss';

const ListErrors = ({errors}) => (
    errors ?
        <ul className="error__messages">
            {
                Object.keys(errors).map(key => {
                    return (
                        <li key={key}>
                            {key} {errors[key]}
                        </li>
                    );
                })
            }
        </ul>
        :
        null
);

export default ListErrors;
