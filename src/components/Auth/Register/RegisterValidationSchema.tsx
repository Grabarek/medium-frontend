import * as Yup from 'yup';

const RegisterValidationSchema = Yup.object().shape({
    username: Yup.string()
        .required('Username is required')
        .matches(/^[a-zA-Z0-9]+$/, 'Username can contain only letters and numbers')
        .min(3, 'Username too short')
        .max(15, 'Username too long'),
    email: Yup.string()
        .required('E-mail address is required')
        .email('Invalid email address'),
    password: Yup.string()
        .required('Password is required')
        .min(8, 'Password has to be at least 8 characters long')
        .matches(/^(?=.*[A-Za-z])(?=.*\d).{8,}$/, 'Password has to contain at least one letter and one number')
});

export default RegisterValidationSchema
