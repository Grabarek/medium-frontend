import React, {useEffect, useState} from 'react';
import {Formik, Form, Field} from 'formik';
import {connect, useDispatch} from 'react-redux';
import {onSubmitFrom, onUnload} from '../../store/settings/actions';

import ListErrors from "../Auth/ListErrors/ListErrors";
import SettingsValidationSchema from "./SettingsValidationSchema";
import passwordIcon from '../../assets/icons/eye.svg';
import profileIcon from '../../assets/icons/profile.svg';
import './Settings.scss';

interface SettingsProps {
    currentUser: User,
    errors: string,
    inProgress: boolean
}

interface User {
    image: string,
    username: string,
    email: string,
    bio: string,
    password: string
}

const mapStateToProps = state => ({
    ...state.settings,
    currentUser: state.commons.currentUser
});

const mapDispatchToProps = () => ({
    onSubmitFrom,
    onUnload
});

const Settings: React.FC<SettingsProps> = ({currentUser, errors, inProgress}) => {
    const [user, setUser] = useState({
        image: '',
        username: '',
        email: '',
        bio: '',
        password: ''
    });
    const [passwordVisibility, setPasswordVisibility] = useState(false);
    const dispatch = useDispatch();

    const addDefaultSrc = (ev) => {
        ev.target.src = profileIcon;
        ev.target.onerror = null;
    };

    const submitForm = values => {
        dispatch(onSubmitFrom(values));
    };

    const initialValues: User = {...user};

    useEffect(() => {
        if (currentUser) {
            const user: User = {
                image: currentUser.image || '',
                username: currentUser.username || '',
                email: currentUser.email || '',
                bio: currentUser.bio || '',
                password: ''
            };
            setUser(user);
        }
    }, [currentUser]);

    useEffect(() => () => dispatch(onUnload()), []);

    return (
        <div className="settings">
            <div className="container">
                <div className="settings__wrapper">
                    <h1 className="settings__title">Settings</h1>

                    <ListErrors errors={errors}/>

                    <Formik
                        initialValues={user}
                        validationSchema={SettingsValidationSchema}
                        onSubmit={values => {
                            submitForm(values)
                        }}
                        enableReinitialize
                    >
                        {({errors, touched}) => (
                            <Form className="settings__form">
                                <div className="settings__form-row">
                                    <img
                                        className="image__preview"
                                        src={`${user.image ? user.image : profileIcon}`}
                                        onError={(e) => addDefaultSrc(e)}
                                    />
                                    <Field
                                        className="image"
                                        name="image"
                                        placeholder="URL of profile image"
                                    />
                                    {errors.image && touched.image &&
                                    <div className="settings__form-error">{errors.image}</div>}
                                </div>

                                <div className="settings__form-row">
                                    <Field
                                        name="username"
                                        placeholder="Username"
                                    />
                                    {errors.username && touched.username &&
                                    <div className="settings__form-error">{errors.username}</div>}
                                </div>

                                <div className="settings__form-row">
                                    <Field
                                        name="bio"
                                        placeholder="Short bio about you"
                                        as="textarea"
                                        rows="6"
                                    />
                                    {errors.bio && touched.bio &&
                                    <div className="settings__form-error">{errors.bio}</div>}
                                </div>

                                <div className="settings__form-row">
                                    <Field
                                        name="email"
                                        placeholder="E-mail address"
                                    />
                                    {errors.email && touched.email &&
                                    <div className="settings__form-error">{errors.email}</div>}
                                </div>

                                <div className="settings__form-row">
                                    <img
                                        src={passwordIcon}
                                        alt="reveal password"
                                        className={`password__reveal ${passwordVisibility && 'active'}`}
                                        onClick={() => setPasswordVisibility(!passwordVisibility)}
                                    />
                                    <Field
                                        name="password"
                                        placeholder="New password"
                                        type={passwordVisibility ? 'text' : 'password'}
                                    />
                                    {errors.password && touched.password &&
                                    <div className="settings__form-error">{errors.password}</div>}
                                </div>


                                <button
                                    type="submit"
                                    className="settings__form-submit"
                                    disabled={inProgress}
                                >
                                    Update settings
                                </button>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
