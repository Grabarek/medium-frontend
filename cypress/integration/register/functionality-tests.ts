describe('Register component functionality tests', function () {

    let generate_random_string = () => {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    };

    let randomString = generate_random_string();


    it('Should get response if data is already taken and display error message', function () {
        cy
            .visit('http://localhost:3001/register')
            .get('.register')
            .get('input[name="username"]')
            .type('username')
            .should('have.value', 'username')
            .get('input[name="email"]')
            .type('test@gmail.com')
            .should('have.value', 'test@gmail.com')
            .get('input[name="password"]')
            .type('PasswordText123')
            .should('have.value', 'PasswordText123')
            .get('button[type="submit"]')
            .click()
            .get('.error__messages')
            .contains('already taken')
            .url().should('include', 'register')
    });

    it('Should keep inputs filled after receiving data already taken error', function () {
        cy
            .get('input[name="username"]')
            .invoke('val').should('not.be.empty')
            .get('input[name="email"]')
            .invoke('val').should('not.be.empty')
            .get('input[name="password"]')
            .invoke('val').should('not.be.empty')
    });

    it('Should successfully register new account without errors', function () {
        cy
            .visit('http://localhost:3001/register')
            .get('.register')
            .get('input[name="username"]')
            .type(`test${randomString}`)
            .should('have.value', `test${randomString}`)
            .get('input[name="email"]')
            .type(`test${randomString}@gmail.com`)
            .should('have.value', `test${randomString}@gmail.com`)
            .get('input[name="password"]')
            .type('PasswordText123')
            .should('have.value', 'PasswordText123')
            .get('button[type="submit"]')
            .click()
            .get('.register__form-error')
            .should('not.exist')
            .get('.error__messages')
            .should('not.exist')
    });

    it('Should automatically redirect to homepage on register', function () {
        cy
            .url().should('not.include', 'register')
            .get('.home')
    });

    it('Should automatically log in and keep user data in store', function () {
        cy
            .wait(1000)
            .window()
            .its('store')
            .invoke('getState')
            .its('commons.currentUser.username')
            .should('exist')
    });



});
