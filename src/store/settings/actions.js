import types from './types.js';
import agent from '../../agent';

const onSubmitFrom = (user) => {
    return {
        type: types.SETTINGS_SAVED,
        payload: agent.Auth.save(user)
    }
};

const onUnload = () => {
    return {
        type: types.SETTINGS_PAGE_UNLOADED,
    }
};

export {
    onSubmitFrom,
    onUnload
}
