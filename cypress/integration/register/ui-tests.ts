describe('Register component UI tests', function () {

    it('Renders register component', function () {
        cy.visit('http://localhost:3001/register');
        cy.get('.register');
    });

    it('Renders h1 title', function () {
        cy.get('h1');
        expect('h1').to.have.length.of.at.least(1);
    });

    it('Renders link to login page', function () {
        cy.get('a[href="/login"]');
        expect('.register__redirect').to.have.length.of.at.least(1);
    });


    it('Renders three input fields', () => {
        cy.get('input').should('have.length', 3);
    });

    it('Renders username input field', () => {
        cy.get('input[name="username"]')
    });

    it('Username input is writable', () => {
        cy
            .get('input[name="username"]')
            .type('username')
            .should('have.value', 'username');
    });

    it('Renders email input field', () => {
        cy.get('input[name="email"]')
    });


    it('Email input is writable', () => {
        cy
            .get('input[name="email"]')
            .type('test@gmail.com')
            .should('have.value', 'test@gmail.com');
    });

    it('Renders password input field', () => {
        cy.get('input[name="password"]')
    });

    it('Password input is writable', () => {
        cy
            .get('input[name="password"]')
            .type('PasswordText123')
            .should('have.value', 'PasswordText123');
    });

    it('Renders password masking/unmasking icon', () => {
        cy.get('.password__reveal');
    });

    it('Triggers password input unmasking', () => {
        cy
            .get('.password__reveal')
            .click()
            .should('have.class', 'active')
            .not('input[name="password"]')
    });

    it('Triggers password input masking', () => {
        cy
            .get('.password__reveal')
            .click()
            .should('have.class', 'false')
            .get('input[name="password"]')
    });


    it('Renders submit button', () => {
        cy
            .get('button[type="submit"]')
    });


});
