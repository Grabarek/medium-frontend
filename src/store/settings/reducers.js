import types from './types';

const INITIAL_STATE = {};

const settings_reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.SETTINGS_SAVED:
            return {
                ...state,
                inProgress: false,
                errors: action.error ? action.payload.errors : null
            };
        case types.SETTINGS_PAGE_UNLOADED:
            return {};
        case types.ASYNC_START:
            return {
                ...state,
                inProgress: true
            };
        case types.ASYNC_END:
            return {
                ...state,
                inProgress: false
            };
        default:
            return state;
    }
};

export default settings_reducer;
