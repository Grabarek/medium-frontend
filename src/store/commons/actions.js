import types from './types.js';

const onLoad = (payload, token) => {
    return {
        type: types.APP_LOAD,
        payload,
        token
    }
};

const onRedirect = () => {
    return {
        type: types.REDIRECT,
    }
};

export {
    onLoad,
    onRedirect
}
